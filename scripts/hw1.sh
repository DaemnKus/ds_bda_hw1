#!/bin/bash

echo "Prepare input and output dirs.."
hdfs dfs -rm -r bu-input
hdfs dfs -rm -r bu-output
hadoop fs -mkdir -p bu-input
hdfs dfs -put TestData/Test*.txt bu-input/

echo "Starting Browser Users job.."
hadoop jar hw1-1.0.jar BrowserUsers bu-input bu-output

echo "Results:"
hdfs dfs -cat bu-output/part-r-00000
rm -f part-r-00000 2> /dev/null
rm -f bu.csv 2> /dev/null
hadoop fs -get bu-output/part-r-00000
mv part-r-00000 bu.csv
