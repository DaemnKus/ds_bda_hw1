Build and deploy manual
=======================

## Required tools

- jdk (version 7 or later)
- maven
- docker
- git

## Preparation

Create working directory

```
mkdir hadoop-browser-users
cd hadoop-browser-users
```

Download sources

```
git clone https://bitbucket.org/DaemnKus/ds_bda_hw1
git clone https://github.com/kiwenlau/hadoop-cluster-docker
```

Download hadoop docker image and create hadoop network

```
docker pull kiwenlau/hadoop:1.0
docker network create --driver=bridge hadoop
```

## Build

Build application with maven

```
cd ds_bda_hw1
mvn package
cd ..
```

Alternatively : [download jar file](https://bitbucket.org/DaemnKus/ds_bda_hw1/downloads/hw1-1.0.jar)

## Deploy

### Prepare containers

Start docker containers

```
cd hadoop-cluster-docker
./start-container.sh
cd ..
```
You shold receive the following output and get into master's shell

```
start hadoop-master container...
start hadoop-slave1 container...
start hadoop-slave2 container...
```

Start hadoop inside master container

```
./start-hadoop.sh
```

### Deploy

Open a new terminal with host shell and cd into `hadoop-browser-users` directory

Change directory to `scripts`

```
cd ds_bda_hw1
cd scripts
```

If you want to generate new input data file you may run script [`generateInput.sh`](./scripts/generateInput.sh). 

```
./generateInput.sh
```
It will generate new file and move it into [`TestData`](./scripts/TestData) directory. Alternatively you can use your own input data and put it into [`TestData`](./scripts/TestData) directory


Run script prepairing your data. It will copy jar file, [`TestData`](./scripts/TestData) directory with input data files and script [`hw1.sh`](./scripts/hw1.sh) for running MapReduce process to docker container

```
./prepareData.sh
```

### Test / Run

Switch to hadoop master container's shell

Run script [`hw1.sh`](./scripts/hw1.sh)
```
./hw1.sh
```
It will create input directory in hdfs and put into it input data files, run browser users job and print the result
