Screenshots
===========

## Screenshot of successfully executed test

![Mapper and reducer tests with maven](./1_Screenshot_Test.png)

## Screenshot of successfully uploaded file into HDFS

![Screenshot of successfully uploaded file into HDFS](./2_Screenshot_UploadedFiles.png)

## Screenshot of successfully executed job and result

![Screenshot of successfully executed job and result 1](./3.1_Screenshot_ExecutionAndResult1.png)

![Screenshot of successfully executed job and result 2](./3.2_Screenshot_ExecutionAndResult2.png)

![Screenshot of successfully executed job and result 3](./3.3_Screenshot_ExecutionAndResult3.png)

## Screenshot with information about memory consumption

### When idle

![Usage when idle](./4.1_Screenshot_Idle.png)

### Right after starting

![Usage after start](./4.2_Screenshot_AfterRunning.png)

### During Map stage

![Usage during Map](./4.3_Screenshot_DuringMap.png)

### During Reduce stage

![Usage during Map](./4.4_Screenshot_DuringReduce.png)
